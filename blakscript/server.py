import socket

PORT = 5005
INIT_TIMEOUT = 40000
IRRADIATE_TIMEOUT = 80000


def start():
    log("Starting server on port " + str(PORT))
    global sock
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('0.0.0.0', PORT))
    sock.listen(1)

    global connection
    connection, address = sock.accept()
    log("Connected to " + str(address[0]) + ":" + str(address[1]))
    while True:
        try:
            buffer = connection.recv(1024).decode("ascii")
            expected_len_data = int(buffer.split(";")[0])
            data = buffer[buffer.find(";") + 1:]
            while len(data) < expected_len_data:
                buffer = connection.recv(1024).decode('ascii')
                data += buffer

            log("length of data: " + str(len(data)))
            data = data.split(';')
            if data[0] == "initialize":
                log("Start initialization")
                response = initialize()
                log(response)
                send_result(response)
            elif data[0] == "bpsIrradiate":
                log("Start bpsIrradiate")
                record = bpsIrradiate(*conversion(*data[1:]))
                attempt(resetLog)
                send_result(str(record))
            elif data[0] == "stop":
                connection.send("stop; ".encode('ascii'))
                break
            else:
                answer = "Don't find the command: " + data[0]
                send_result(answer)
        except Exception as e:
            log(str(e))
            break

    stop()


def stop():
    log("Closing connection")
    connection.send("closing connection".encode("ascii"))
    connection.close()
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()


def send_result(result):
    to_send = str(len(result))+";"+result
    log("length to send "+str(len(result)))
    while len(to_send) > 1000:
        paket = to_send[:1000]
        to_send = to_send[1000:]
        connection.send(paket.encode("ascii"))

    connection.send(to_send.encode('ascii'))


def initialize():
    log("    >> Initialisation\n")
    init_chain = Chain("Initialize") \
        .add(activate, "q1c") \
        .add(activate, "q2c") \
        .add(setTargetOpening, "sl1e", 40) \
        .add(moveDegraderTo, "BeamStop") \
        .add(selectBdp) \
        .add(iseuChainClose) \
        .auto_skip()

    init_chain.parallel_play(INIT_TIMEOUT)

    if init_chain.is_success():
        return "Sucess; "
    else:
        return "Failed; " + init_chain.get_fail_reason()


def conversion(vdee, iarc):
    vdee = vdee.replace(" ", "")
    if "[" in vdee:
        vdee_sp = string_to_list(vdee)
    else:
        vdee_sp = float(vdee)

    iarc = iarc.replace(" ", "")
    if "[" in iarc:
        iarc_sp = string_to_list(iarc)
    else:
        iarc_sp = float(iarc)

    return vdee_sp, iarc_sp


def string_to_list(string_arg):
    string_arg = string_arg.replace("[", "")
    string_arg = string_arg.replace("]", "")
    result = []
    if "." in string_arg:
        for s in string_arg.split(","):
            result.append(float(s))
    else:
        for s in string_arg.split(","):
            result.append(int(s))

    return result


def bpsIrradiate(vdee_sp, iarc_sp):
    log("    >> Shooting irradiation")
    irradiate_chain = Chain("Irradiate")

    if type(vdee_sp) is list and type(iarc_sp) is list:
        irradiate_chain = irradiate_chain.add(prepareSpotsWithDifferentIArcs, 0.0, iarc_sp, vdee_sp)
    else:
        log("    >> ERROR: bpsIrradiation wrong arguments type")
        return "Failed; vdee and iarc set points are not list type"

    irradiate_chain = irradiate_chain.add(irradiateBurst) \
        .add(retrieveLog, ["IC_CYCLO_LARGE_GAP_SIDEB_MIN", "DEGRADER_BEAMSTOP", "SRSEU_SETPOINT_FEEDBACK_VDEE",
                           "SRSEU_SETPOINT_FEEDBACK_IARC"]) \
        .auto_skip()
    record = irradiate_chain.play(IRRADIATE_TIMEOUT)

    if irradiate_chain.is_success():
        return record
    else:
        return "Failed; " + irradiate_chain.get_fail_reason()
