import logging
from pathlib import Path
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QFont
import json
from ftovopt.controller.controller import Controller
from ftovopt.logic.bdcu_jmx import BdcuJMX
from ftovopt.logic.ftog_generator import FtoGGenerator
from ftovopt.logic.ftov_generator import FtoVGenerator
from ftovopt.logic.srseu import Srseu
from ftovopt.view.main_window import MainWindow
from ftovopt.logic.ghost_beam import GhostBeam

logger = logging.getLogger(__name__)


class FtoVOpt(QApplication):
    def __init__(self):
        super(FtoVOpt, self).__init__(sys.argv)

        self.setStyle("Fusion")
        self.setFont(QFont("Bahnschrift SemiLight", 9))
        
        configFile=Path(__file__).resolve().parent.parent / "config" / "config.json"
        
        with open(configFile) as configJson:
            config = json.load(configJson)

        self.ftov_generator=FtoVGenerator()
        self.ftog_generator=FtoGGenerator()
        self.srseu=Srseu(config)
        self.bdcu=BdcuJMX(config)
        self.ghost_beam=GhostBeam(config)

        self.controller = Controller(config,self.ftov_generator,self.ftog_generator,self.srseu,self.bdcu,self.ghost_beam)
        self.view = None

    def start_graphical_interface(self):
        logger.info("Start Graphical interface")
        self.view = MainWindow(self.controller)
        self.view.show()

        self.exec()
        self.stop_app()

    def start_comand_line_interface(self):
        pass

    def stop_app(self):
        logger.info("Stop application")
        if self.controller.thread().isRunning():
            logger.info(f"Stopping controller")
            self.controller.thread().quit()
            self.controller.thread().wait()

