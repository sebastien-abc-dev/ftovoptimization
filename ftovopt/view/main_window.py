from PyQt5.QtCore import QPoint, QLocale
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QToolBar, QStatusBar, QMenu, QAction, QVBoxLayout, \
    QHBoxLayout, QTableView, QFileDialog, QSizePolicy, QHeaderView, QInputDialog, QDialog, QGroupBox, QSpinBox, \
    QFormLayout, QDialogButtonBox, QDesktopWidget

from PyQt5 import QtCore
from ftovopt.view.parameters_table_model import ParametersTableModel
import pyqtgraph as pg

from ftovopt.controller.controller import Controller
from ftovopt.view import PATH_RESOURCES

class MainWindow(QMainWindow):

    REFRESH_PERIOD_MS=100
    def __init__(self,controller: Controller):
        super().__init__()

        self.controller = controller
        self.controller.addListener(self)

        self.setWindowTitle("S2C2 FtoV Optimization")
        path = PATH_RESOURCES / "iba.ico"
        self.setWindowIcon(QIcon(str(path)))

        self.centralWidget = QWidget(self)
        self.setCentralWidget(self.centralWidget)

        self.centralLayout = QHBoxLayout()
        self.centralWidget.setLayout(self.centralLayout)

        self.buildParametersTable()

        self.centralLayout.addWidget(self.parametersTableView)

        self.rightWidget=QWidget(self)
        self.rightLayout=QVBoxLayout()
        self.rightWidget.setLayout(self.rightLayout)

        self.centralLayout.addWidget(self.rightWidget)

        self.inputPlotWidget=QWidget(self)
        self.inputPlotLayout=QHBoxLayout()
        self.inputPlotWidget.setLayout(self.inputPlotLayout)

        self.buildFrequencyPlot()        
        self.inputPlotLayout.addWidget(self.frequencyPlot)

        self.buildTimePlot()
        self.inputPlotLayout.addWidget(self.timePlot)

        self.rightLayout.addWidget(self.inputPlotWidget)
        self.buildGhostBeamPlot()
        self.rightLayout.addWidget(self.ghostBeamPlot)

        #self.refreshGuiTimer = QtCore.QTimer()
        #self.refreshGuiTimer.setInterval(MainWindow.REFRESH_PERIOD_MS)
        #self.refreshGuiTimer.timeout.connect(self.refreshGui)
        #self.refreshGuiTimer.start()

        self.createMenu()
        self.createStatusBar()

    def createMenu(self):
        self.menuBar=self.menuBar()
        self.acquisitionMenu=QMenu("&Acquisition",self)
        startAcquisitionAction = QAction("Start", self)
        self.acquisitionMenu.addAction(startAcquisitionAction)
        startAcquisitionAction.triggered.connect(self.startAcquisition)
        
        stopAcquisitionAction = QAction("Stop", self)
        self.acquisitionMenu.addAction(stopAcquisitionAction)
        stopAcquisitionAction.triggered.connect(self.stopAcquisition)

        loadFDeeAction = QAction("Load FDee", self)
        self.acquisitionMenu.addAction(loadFDeeAction)
        loadFDeeAction.triggered.connect(self.loadFDee)

        self.acquisitionMenu.addAction('&Exit', self.close)

        self.menuBar.addMenu(self.acquisitionMenu)

        self.ftoVMenu=QMenu("&FtoV",self)

        loadFtoVFromFileAction = QAction("Load FtoV from file", self)
        self.ftoVMenu.addAction(loadFtoVFromFileAction)
        loadFtoVFromFileAction.triggered.connect(self.loadFtoVFromFile)

        loadFtoVFromSRSEUAction = QAction("Load FtoV from SRSEU", self)
        self.ftoVMenu.addAction(loadFtoVFromSRSEUAction)
        loadFtoVFromSRSEUAction.triggered.connect(self.loadFtoVFromSRSEU)

        saveFtoVToFileAction = QAction("Save FtoV to file", self)
        self.ftoVMenu.addAction(saveFtoVToFileAction)
        saveFtoVToFileAction.triggered.connect(self.saveFtoVToFile)

        sendFtoVToSRSEUAction = QAction("Send FtoV to SRSEU", self)
        self.ftoVMenu.addAction(sendFtoVToSRSEUAction)
        sendFtoVToSRSEUAction.triggered.connect(self.sendFtoVToSRSEU)

        generateRandomFtoVAction = QAction("Generate Random", self)
        self.ftoVMenu.addAction(generateRandomFtoVAction)
        generateRandomFtoVAction.triggered.connect(self.generateRandomFtoV)
        self.menuBar.addMenu(self.ftoVMenu)


        generateFlatFtoVAction = QAction("Generate Flat", self)
        self.ftoVMenu.addAction(generateFlatFtoVAction)
        generateFlatFtoVAction.triggered.connect(self.generateFlatFtoV)
        self.menuBar.addMenu(self.ftoVMenu)

        generateBezierFtoVAction = QAction("Generate Bezier", self)
        self.ftoVMenu.addAction(generateBezierFtoVAction)
        generateBezierFtoVAction.triggered.connect(self.generateBezierFtoV)
        self.menuBar.addMenu(self.ftoVMenu)

        self.ftoGMenu=QMenu("&FtoG",self)

        loadFtoGFromFileAction = QAction("Load FtoG from file", self)
        self.ftoGMenu.addAction(loadFtoGFromFileAction)
        loadFtoGFromFileAction.triggered.connect(self.loadFtoGFromFile)

        loadFtoGFromSRSEUAction = QAction("Load FtoG from SRSEU", self)
        self.ftoGMenu.addAction(loadFtoGFromSRSEUAction)
        loadFtoGFromSRSEUAction.triggered.connect(self.loadFtoGFromSRSEU)

        saveFtoGToFileAction = QAction("Save FtoG to file", self)
        self.ftoGMenu.addAction(saveFtoGToFileAction)
        saveFtoGToFileAction.triggered.connect(self.saveFtoGToFile)

        sendFtoGToSRSEUAction = QAction("Send FtoG to SRSEU", self)
        self.ftoGMenu.addAction(sendFtoGToSRSEUAction)
        sendFtoGToSRSEUAction.triggered.connect(self.sendFtoGToSRSEU)

        generateFlatFtoGAction = QAction("Generate Flat", self)
        self.ftoGMenu.addAction(generateFlatFtoGAction)
        generateFlatFtoGAction.triggered.connect(self.generateFlatFtoG)

        self.menuBar.addMenu(self.ftoGMenu)

        generateTriangleFtoGAction = QAction("Generate Triangle", self)
        self.ftoGMenu.addAction(generateTriangleFtoGAction)
        generateTriangleFtoGAction.triggered.connect(self.generateTriangleFtoG)

        self.menuBar.addMenu(self.ftoGMenu)

        self.srseuMenu=QMenu("&SRSEU",self)
        fetchAllParametersAction = QAction("Fetch all parameters", self)
        self.srseuMenu.addAction(fetchAllParametersAction)
        fetchAllParametersAction.triggered.connect(self.fetchAllParametersSrseu)

        setVdeeAttSrseuAction = QAction("Set VDee Att", self)
        self.srseuMenu.addAction(setVdeeAttSrseuAction)
        setVdeeAttSrseuAction.triggered.connect(self.setVdeeAttSrseu)

        self.menuBar.addMenu(self.srseuMenu)
        sendFtoGAction = QAction("Set VDee Att", self)
        self.srseuMenu.addAction(setVdeeAttSrseuAction)
        setVdeeAttSrseuAction.triggered.connect(self.setVdeeAttSrseu)

        self.menuBar.addMenu(self.srseuMenu)


        self.bdcuMenu=QMenu("&BDCU",self)

        connectBdcuAction = QAction("Connect", self)
        self.bdcuMenu.addAction(connectBdcuAction)
        connectBdcuAction.triggered.connect(self.connectBdcu)

        initializeBdcuAction = QAction("Initialize", self)
        self.bdcuMenu.addAction(initializeBdcuAction)
        initializeBdcuAction.triggered.connect(self.initializeBdcu)

        playMapAction = QAction("Play map", self)
        self.bdcuMenu.addAction(playMapAction)
        playMapAction.triggered.connect(self.playMap)

        closeBdcuAction = QAction("Close", self)
        self.bdcuMenu.addAction(closeBdcuAction)
        closeBdcuAction.triggered.connect(self.closeBdcu)


        self.menuBar.addMenu(self.bdcuMenu)

        self.menuBar.setNativeMenuBar(False)
        self.setMenuBar(self.menuBar)


    def createStatusBar(self):
        status = QStatusBar()
        status.showMessage("FtoV Optimization")
        self.setStatusBar(status)


    def startAcquisition(self):
        [validDialog,nbAcquisitions]=self.showStartIrradiationDialog()
        if (validDialog):
            self.controller.startAcquisition(nbAcquisitions)

    def stopAcquisition(self):
        self.controller.stopAcquisition()

    def generateRandomFtoV(self):
        self.controller.generateRandomFtoV()

    def generateFlatFtoV(self):
        num,ok = QInputDialog.getInt(self,"Amplitude","Enter amplitude (0-50000)")
        self.controller.generateFlatFtoV(num)

    def generateFlatFtoG(self):
        dialog= QInputDialog()
        dialog.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        dialog.setInputMode(QInputDialog.DoubleInput)
        dialog.setWindowTitle("Gain")
        dialog.setLabelText("Enter gain (-12.5 - 12.5)")
        dialog.setDoubleMinimum(-12.5)
        dialog.setDoubleMaximum(12.5)
        dialog.setDoubleStep(0.1)

        dialog.exec()
        gain=dialog.doubleValue()
        self.controller.generateFlatFtoG(gain)

    def generateTriangleFtoG(self):
        dialog= QInputDialog()
        dialog.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        dialog.setInputMode(QInputDialog.DoubleInput)
        dialog.setDoubleMinimum(-12.5)
        dialog.setDoubleMaximum(12.5)
        dialog.setDoubleStep(0.1)

        dialog.setWindowTitle("Triangle FtoG")
        dialog.setLabelText("Enter min gain (-12.5 - 12.5)")
        dialog.exec()
        min_gain=dialog.doubleValue()

        dialog= QInputDialog()
        dialog.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        dialog.setInputMode(QInputDialog.DoubleInput)
        dialog.setDoubleMinimum(-12.5)
        dialog.setDoubleMaximum(12.5)
        dialog.setDoubleStep(0.1)
        dialog.setWindowTitle("Triangle FtoG")
        dialog.setLabelText("Enter max gain (-12.5 - 12.5)")
        dialog.exec()
        max_gain=dialog.doubleValue()

        self.controller.generateTriangleFtoG(min_gain,max_gain)

    def generateBezierFtoV(self):
        self.controller.generateBezierFtoV()

    def loadFtoVFromFile(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setAcceptMode(QFileDialog.AcceptOpen)
        dlg.setNameFilter("FtoV (*.csv)")
        filenames = None

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if (len(filenames)==1):
                filename=filenames[0]
                self.controller.loadFtoVFromFile(filename)

    def loadFtoGFromFile(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setAcceptMode(QFileDialog.AcceptOpen)
        dlg.setNameFilter("FtoG (*.csv)")
        filenames = None

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if (len(filenames)==1):
                filename=filenames[0]
                self.controller.loadFtoGFromFile(filename)

    def loadFtoVFromSRSEU(self):
        self.controller.loadFtoVFromSRSEU()

    def loadFtoGFromSRSEU(self):
        self.controller.loadFtoGFromSRSEU()

    def saveFtoVToFile(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptSave )
        dlg.setNameFilter("FtoV (*.csv)")
        filenames = None

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if (len(filenames)==1):
                filename=filenames[0]
                self.controller.saveFtoVToFile(filename)

    def saveFtoGToFile(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptSave )
        dlg.setNameFilter("FtoG (*.csv)")
        filenames = None

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if (len(filenames)==1):
                filename=filenames[0]
                self.controller.saveFtoGToFile(filename)

    def sendFtoVToSRSEU(self):
        self.controller.sendFtoVToSRSEU()

    def sendFtoGToSRSEU(self):
        self.controller.sendFtoGToSRSEU()


    def loadFDee(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setAcceptMode(QFileDialog.AcceptOpen)
        dlg.setNameFilter("Numpy file (*.npy)")
        filenames = None

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if (len(filenames)==1):
                filename=filenames[0]
                self.controller.loadFDee(filename)


    def connectBdcu(self):
        self.controller.connectBdcu()

    def initializeBdcu(self):
        self.controller.initializeBdcu()

    def playMap(self):
        self.controller.playMap()

    def closeBdcu(self):
        self.controller.closeBdcu()


    def fetchAllParametersSrseu(self):
        self.controller.fetchAllParametersSrseu()

    def setVdeeAttSrseu(self):
        num,ok = QInputDialog.getInt(self,"Amplitude","Enter amplitude (0-50000)")
        self.controller.setVdeeAttSrseu(num)

    def buildFrequencyPlot(self):
        self.frequencyPlot = pg.PlotWidget()
        self.frequencyPlotItem=self.frequencyPlot.plotItem
        self.frequencyPlotItem.setLabels(left='Voltage')
        self.frequencyPlotItem.getAxis('left').setLabel('Volt',units='V')
        self.frequencyPlotItem.showGrid(x = True, y = True, alpha = 0.3)
        self.ftoVLine = pg.PlotCurveItem([],[],pen=pg.mkPen(color=(255, 0, 0)))
        self.ftoVFilteredLine = pg.PlotCurveItem([],[],pen=pg.mkPen(color=(0, 0, 255)))
        self.frequencyPlotItem.addItem(self.ftoVLine)
        self.frequencyPlotItem.addItem(self.ftoVFilteredLine)

        self.frequencyPlotItem2 = pg.ViewBox()
        self.frequencyPlotItem.showAxis('right')
        self.frequencyPlotItem.scene().addItem(self.frequencyPlotItem2)
        self.frequencyPlotItem.getAxis('right').linkToView(self.frequencyPlotItem2)
        self.frequencyPlotItem2.setXLink(self.frequencyPlotItem)
        self.frequencyPlotItem.getAxis('right').setLabel('Gain', units='%')
        self.ftoGLine = pg.PlotCurveItem([], [], pen=pg.mkPen(color=(0, 255, 0)))
        self.frequencyPlotItem2.addItem(self.ftoGLine)

        self.frequencyPlot.setLabel('bottom', "Frequency", units='Hz')

    def buildTimePlot(self):
        self.timePlot = pg.PlotWidget()
        self.timePlotItem = self.timePlot.plotItem
        self.timePlotItem.setLabels(left='Frequency')
        self.timePlotItem.getAxis('left').setLabel('Frequency',units='Hz')
        self.timePlotItem.showGrid(x = True, y = True, alpha = 0.3) 
        self.ttoFLine = self.timePlot.plot([],[], pen=pg.mkPen(color=(0, 255, 0)))

        self.timePlotItem2 = pg.ViewBox()
        self.timePlotItem.showAxis('right')
        self.timePlotItem.scene().addItem(self.timePlotItem2)
        self.timePlotItem.getAxis('right').linkToView(self.timePlotItem2)
        self.timePlotItem2.setXLink(self.timePlotItem)
        self.timePlotItem.getAxis('right').setLabel('Voltage',units='V')
        self.ttoVLine=pg.PlotCurveItem([],[],pen=pg.mkPen(color=(255, 0, 0)))
        self.timePlotItem2.addItem(self.ttoVLine)
        self.ttoVFilteredLine=pg.PlotCurveItem([],[],pen=pg.mkPen(color=(0, 0, 255)))
        self.timePlotItem2.addItem(self.ttoVFilteredLine)
        
        self.timePlot.setLabel('bottom', "Time",units='s')

    def buildGhostBeamPlot(self):
        self.ghostBeamPlot = pg.PlotWidget()
        self.ghostBeamLine =  self.ghostBeamPlot.plot([],[], pen=pg.mkPen(color=(255, 0, 0)))
        self.ghostBeamReconstructedFitLine=self.ghostBeamPlot.plot([],[], pen=pg.mkPen(color=(0, 255, 0)))
        self.ghostBeamPlot.setLabel('bottom', "Time", units='s')
        self.ghostBeamPlot.showAxis('left')
        self.ghostBeamPlot.getAxis('left').setLabel('Charge/pulse - protons',units='C')
        self.ghostBeamPlot.showGrid(x = True, y = True, alpha = 0.3) 


    def buildParametersTable(self):
        self.parametersTableView = QTableView()
        self.parametersTableModel = ParametersTableModel(self.controller.getGuiParameters())
        self.parametersTableView.setModel(self.parametersTableModel)
        self.parametersTableView.resizeColumnsToContents()

    def refreshGui(self):
        currentFtoVData=self.controller.getCurrentFtoVData()

        if (not currentFtoVData is None):
            self.ftoVLine.setData((currentFtoVData[:,1]*1E6).tolist(), currentFtoVData[:,2].tolist())
            self.ttoFLine.setData(currentFtoVData[:,0].tolist(), (currentFtoVData[:,1]*1E6).tolist())
            self.ttoVLine.setData(currentFtoVData[:,0].tolist(), currentFtoVData[:,2].tolist())
            self.ttoVFilteredLine.setData(currentFtoVData[:,0].tolist(), currentFtoVData[:,3].tolist())
            self.ftoVFilteredLine.setData((currentFtoVData[:,1]*1E6).tolist(), currentFtoVData[:,3].tolist())
            self.timePlotItem2.setGeometry(self.timePlotItem.vb.sceneBoundingRect())
            self.timePlotItem2.linkedViewChanged(self.timePlotItem.vb, self.timePlotItem2.XAxis)

        currentFtoGData=self.controller.getCurrentFtoGData()

        if (not currentFtoGData is None):
            self.ftoGLine.setData((currentFtoGData[:,0]*1E6).tolist(), currentFtoGData[:,1].tolist())
            self.frequencyPlotItem2.setGeometry(self.frequencyPlotItem.vb.sceneBoundingRect())
            self.frequencyPlotItem2.linkedViewChanged(self.frequencyPlotItem.vb, self.frequencyPlotItem2.XAxis)


        currentGhostBeamMeasure=self.controller.getCurrentGhostBeamMeasure()

        if (not currentGhostBeamMeasure is None):
            self.ghostBeamLine.setData(currentGhostBeamMeasure[:,0].tolist(),currentGhostBeamMeasure[:,1].tolist())
            self.ghostBeamReconstructedFitLine.setData(currentGhostBeamMeasure[:,0].tolist(),currentGhostBeamMeasure[:,2].tolist())

        self.parametersTableModel.update(self.controller.getGuiParameters())


    def onEvent(self):
        self.refreshGui()

    def showStartIrradiationDialog(self):
        dialog=QDialog(self)
        dialog.setGeometry(100, 100, 300, 200)
        size = dialog.size()
        desktopSize = QDesktopWidget().screenGeometry()
        top = (desktopSize.height() / 2) - (size.height() / 2)
        left = (desktopSize.width() / 2) - (size.width() / 2)
        dialog.move(QPoint(int(left), int(top)))
        dialog.setWindowTitle("Start Acquisition...")

        formGroupBox = QGroupBox()
        number_acquisition_spin_box = QSpinBox()
        formLayout=QFormLayout()
        formLayout.addRow(QLabel("Number of acquisitons"), number_acquisition_spin_box)
        formGroupBox.setLayout(formLayout)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(formGroupBox)
        # adding button box to the layout
        mainLayout.addWidget(buttonBox)

        # setting lay out
        dialog.setLayout(mainLayout)
        dialog.setModal(True)
        validDialog=dialog.exec()

        return [validDialog,number_acquisition_spin_box.value()]