from PyQt5 import QtCore

class ParametersTableModel(QtCore.QAbstractTableModel):

    def __init__(self, data): 
        super(ParametersTableModel, self).__init__()
        self.data = data
        self.headers=["Name","Value"]
        self.maxColumnIndex=-1
        self.maxRowIndex=-1

    def update(self, data):
        self.data=data
        self.modelReset.emit()

    def data(self, index, role):
        if (self.data is None):
            return None

        if index.isValid() and role == QtCore.Qt.DisplayRole:
            return str(list(self.data.items())[index.row()][index.column()])
        
    def rowCount(self, index):
        if (self.data is None):
            return 0
        else:
            return len(self.data)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return 2

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.headers[section]
