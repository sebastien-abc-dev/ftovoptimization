import sys
import logging
import logging.config
from pathlib import Path


# Log all uncaught exceptions
def handle_exception(exc_type, exc_value, exc_traceback):
    logging.exception(f"Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def initialize_logging(config_file_path):
    log_path = Path(__file__).resolve().parent.parent.parent/ "logs" / "ftovopt.log"
    print(log_path)
    log_path.parent.mkdir(parents=True, exist_ok=True)
    log_path.touch(exist_ok=True)

    logging.config.fileConfig(config_file_path)

    sys.excepthook = handle_exception
