import logging
from pathlib import Path

from ftovopt import parse_arguments
from ftovopt.config.logger import initialize_logging
from ftovopt.core.app import FtoVOpt

config_file_path = Path(__file__).resolve().parent / "config" / "log.conf"
initialize_logging(config_file_path)

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    args = parse_arguments()
    logger.info("Start application")
    app = FtoVOpt()
    if not args.cli:
        logger.info("Start graphical interface")
        app.start_graphical_interface()
    else:
        logger.info("Start command line interface")
        app.start_comand_line_interface()
    logger.info("Application closed")
