import logging
from pathlib import Path
import json
from logic.bdcu_jmx import BdcuJMX
from config.logger import initialize_logging
import numpy as np

config_file_path = Path(__file__).resolve().parent / "config" / "log.conf"
initialize_logging(config_file_path)

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    configFile = Path(__file__).resolve().parent / "config" / "config.json"
    with open(configFile) as configJson:
        config = json.load(configJson)

    bdcu=BdcuJMX(config)
    bdcu.connect()
    print(bdcu.send_cyclo_burst(np.array([[100,100,100],[8.5,8.5,8.5]]).T))