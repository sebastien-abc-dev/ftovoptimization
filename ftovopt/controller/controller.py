from pathlib import Path
from PyQt5.QtCore import QObject, QThread
import collections
import numpy as np
from ftovopt.logic.bdcu_jmx import BdcuJMX
from ftovopt.logic.ghost_beam import GhostBeam
import time
from threading import Thread

class Controller(QObject):
    def __init__(self, config,ftovGenerator,ftogGenerator, srseu, bdcu: BdcuJMX,ghost_beam: GhostBeam):
        super().__init__()

        self.listeners=[]
        self.config=config

        self.currentGhostBeamMeasure=None
        self.ghostBeamReconstructedFit=None
        self.currentFtoVData=None
        self.currentFtoGData=None
        self.parameters=collections.OrderedDict()
        self.parameters["nb_acquisitions"]=0
        self.ftovGenerator = ftovGenerator
        self.ftogGenerator = ftogGenerator
        self.srseu=srseu
        self.bdcu=bdcu
        self.ghost_beam=ghost_beam

    def startAcquisition(self,nb_acquisitions):
        t = Thread(target=myfunc, args=(i,))
        t.start()
        myThread=Thread()
        print("Starting acquisition")
        for i in range(nb_acquisitions):
            self.generateBezierFtoV()
            time.sleep(1)


    def stopAcquisition(self):
        print("Stopping acquisition")

    def getCurrentFtoVData(self):
        return self.currentFtoVData

    def getCurrentFtoGData(self):
        return self.currentFtoGData

    def getCurrentGhostBeamMeasure(self):
        return self.currentGhostBeamMeasure

    def getGuiParameters(self):
        return self.parameters

    def generateRandomFtoV(self):
        [self.currentRandomValues,self.currentFtoVData]=self.ftovGenerator.generate_random_vdee(self.config["random_ftov"],self.config["srseu"]["parameters"])
        for i in range(len(self.currentRandomValues)):
            self.parameters[f"random_values_{i}"]=self.currentRandomValues[i]
        self.fireEvent()

    def generateFlatFtoV(self,amplitude):
        self.currentFtoVData=self.ftovGenerator.generate_flat_vdee(self.config["srseu"]["parameters"],amplitude)
        self.fireEvent()

    def generateFlatFtoG(self,gain):
        self.currentFtoGData=self.ftogGenerator.generate_flat_ftog(self.config["srseu"]["parameters"],gain)
        self.fireEvent()

    def generateTriangleFtoG(self,min_gain,max_gain):
        self.currentFtoGData=self.ftogGenerator.generate_triangle_ftog(self.config["srseu"]["parameters"],min_gain,max_gain)
        self.fireEvent()

    def generateBezierFtoV(self,):

        [self.currentRandomValues,self.currentFtoVData]=self.ftovGenerator.generate_bezier_vdee(self.config["bezier_ftov"],self.config["srseu"]["parameters"])
        for i in range(len(self.currentRandomValues)):
            self.parameters[f"random_values_{i}"]=self.currentRandomValues[i]
        self.fireEvent()

    def loadFDee(self,filename):
        self.currentFtoVData=self.ftovGenerator.loadFDee(filename,self.config["fdee"]["sampling_period"])
        self.fireEvent()

    def loadFtoVFromFile(self,filename):
        raw_ftov = np.genfromtxt(filename, delimiter=';')
        self.currentFtoVData=self.ftovGenerator.computeVDee(raw_ftov,self.config["srseu"]["parameters"])
        self.fireEvent()

    def loadFtoGFromFile(self,filename):
        raw_ftog = np.genfromtxt(filename, delimiter=';')
        self.currentFtoGData=self.ftogGenerator.computeFtoG(raw_ftog,self.config["srseu"]["parameters"])
        self.fireEvent()

    def loadFtoVFromSRSEU(self):
        raw_ftov=self.srseu.loadFtoV()
        self.currentFtoVData=self.ftovGenerator.computeVDee(raw_ftov,self.config["srseu"]["parameters"])
        self.fireEvent()

    def loadFtoGFromSRSEU(self):
        raw_ftog=self.srseu.loadFtoG()
        self.currentFtoGData=self.ftogGenerator.computeFtoG(raw_ftog,self.config["srseu"]["parameters"])
        self.fireEvent()

    def saveFtoVToFile(self,filename):
        raw_ftov=self.ftovGenerator.extract_ftov(self.currentFtoVData,self.config["srseu"]["parameters"])
        np.savetxt(filename, raw_ftov, delimiter=";",fmt='%i')

    def saveFtoGToFile(self,filename):
        raw_ftog=self.ftogGenerator.extract_ftog(self.currentFtoGData,self.config["srseu"]["parameters"])
        np.savetxt(filename, raw_ftog, delimiter=";",fmt='%i')

    def sendFtoVToSRSEU(self):
        raw_ftov=self.ftovGenerator.extract_ftov(self.currentFtoVData,self.config["srseu"]["parameters"])
        self.srseu.sendFtoV(raw_ftov)

    def sendFtoGToSRSEU(self):
        raw_ftog=self.ftogGenerator.extract_ftog(self.currentFtoGData,self.config["srseu"]["parameters"])
        self.srseu.sendFtoG(raw_ftog)

    def setVdeeAttSrseu(self,amplitude):
        self.srseu.setVdeeAtt(amplitude)

    def connectBdcu(self):
        self.bdcu.connect()

    def initializeBdcu(self):
        pass

    def playMap(self):
        setpoints=self.ghost_beam.build_setpoints()
        print(setpoints)
        logs=self.bdcu.send_cyclo_burst(setpoints)
        (max_charge,self.currentGhostBeamMeasure)=self.ghost_beam.parse_ghost_beam_logs(logs)
        
        self.parameters["Max charge"]=max_charge

        parameters=self.ghost_beam.fit_function(self.currentGhostBeamMeasure)

        self.parameters["Fit parameters"]=parameters

        self.fireEvent()

    def closeBdcu(self):
        self.bdcu.close()

    def fetchAllParametersSrseu(self):
        self.parameters.update(self.srseu.getAllParameters())
        self.fireEvent()
    def addListener(self,listener):
        self.listeners.append(listener)

    def fireEvent(self):
        for listener in self.listeners:
            listener.onEvent()