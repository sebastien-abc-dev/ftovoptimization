__version__ = "0.1.0"
__description__ = "Project for to optimize the FtoV of the S2C2"
__author__ = "shenrotin, ictkint"
__url__ = "https://gitlab.sw.goiba.net/s2c2rfcharacterization/ftovoptimization"
__license__ = "iba"

import argparse


def get_version():
    return f"{__name__} v{__version__}"


def parse_arguments():
    parser = argparse.ArgumentParser(description='Optimization of the FtoV',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--version', action='version', version=get_version(),
                        help='show the application\'s version and exit')
    parser.add_argument('--cli', default=False, action='store_true', help='Start app in command line')

    args, unknown = parser.parse_known_args()
    return args
