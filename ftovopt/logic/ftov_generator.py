from pathlib import Path
import numpy as np
import bezier
import random

class FtoVGenerator:
    def __init__(self):
        pass

    def filter_vdee(self,rf_setpoint,srseu_parameters):
        result=np.zeros(rf_setpoint.shape)
        result[0]=rf_setpoint[0]
        max_delta_slew_rate_v=srseu_parameters["vdee_slew_rate"]/100*self.sampling_period/500E-9
        for i in range(1,len(rf_setpoint)):
            delta_v=rf_setpoint[i]-result[i-1]
            if(delta_v>max_delta_slew_rate_v):
                result[i]=result[i-1]+max_delta_slew_rate_v
            elif (delta_v<-max_delta_slew_rate_v):
                result[i]=result[i-1]-max_delta_slew_rate_v
            else:
                result[i]=rf_setpoint[i]
        return result

    def loadFDee(self,filename,sampling_period):
        self.fdee=np.load(filename)
        self.sampling_period=sampling_period
        self.time_vector=np.arange(0,1E-3,self.sampling_period)[0:len(self.fdee)]
        return np.vstack([self.time_vector,self.fdee,np.zeros((2,len(self.time_vector)))]).T

    def computeVDee(self,raw_ftov,srseu_parameters):
        ftov=np.zeros(raw_ftov.shape)
        ftov[:,0]=(srseu_parameters["fdee_base_index"]+raw_ftov[:,0])/100
        ftov[:,1]=raw_ftov[:,1]/srseu_parameters["voltage_factor"]
    
        #Interpolation from (t,F) and (F,V) to get (t,V)
        vdee=np.interp(self.fdee,ftov[:,0],ftov[:,1])
   
        #RF start and stop
        start_rf_index=int(srseu_parameters["vdee_delay"]*1E-6/self.sampling_period)
        end_rf_index=np.argmin(self.fdee)
    
        #Filter vdee
        rf_setpoint=vdee.copy()
        rf_setpoint[0:start_rf_index]=srseu_parameters["vdee_atten"]/srseu_parameters["voltage_factor"]
        rf_setpoint[end_rf_index:]=srseu_parameters["vdee_atten"]/srseu_parameters["voltage_factor"]

        filtered_vdee=self.filter_vdee(rf_setpoint,srseu_parameters)
            
        return np.vstack([self.time_vector,self.fdee,rf_setpoint,filtered_vdee]).T

    def generate_random_vdee(self,random_ftov_parameters,srseu_parameters):
        #Generate stepped random vdee
        start_rf_index=int(srseu_parameters["vdee_delay"]*1E-6/self.sampling_period)
        end_rf_index=np.argmin(self.fdee)

        nb_samples_per_step=int((end_rf_index-start_rf_index)/random_ftov_parameters["nb_steps"])
        rf_setpoint=np.ones(len(self.fdee))*srseu_parameters["vdee_atten"]/srseu_parameters["voltage_factor"]
        random_values=random_ftov_parameters["min_vdee"]+np.random.rand(random_ftov_parameters["nb_steps"],1)*(random_ftov_parameters["max_vdee"]-random_ftov_parameters["min_vdee"])
        for i in range(random_ftov_parameters["nb_steps"]):
            rf_setpoint[start_rf_index+i*nb_samples_per_step:start_rf_index+(i+1)*nb_samples_per_step]=random_values[i]
        
        #Filter the random values
        filtered_vdee=self.filter_vdee(rf_setpoint,srseu_parameters)
        return random_values,np.vstack([self.time_vector,self.fdee,rf_setpoint,filtered_vdee]).T

    def generate_flat_vdee(self,srseu_parameters,amplitude):
        frequencies_digits=np.arange(3001)
        voltages=np.ones(3001)*amplitude
        raw_ftov=np.vstack([frequencies_digits,voltages]).T.astype('int')
        return self.computeVDee(raw_ftov,srseu_parameters)

    def generate_bezier_vdee(self,bezier_ftov_parameters,srseu_parameters):
        start_rf_index=int(srseu_parameters["vdee_delay"]*1E-6/self.sampling_period)
        end_rf_index=np.argmin(self.fdee)

        random_t = np.random.randint(start_rf_index, end_rf_index, bezier_ftov_parameters["nb_steps"]-2)
        random_t = np.append(random_t, [start_rf_index, end_rf_index])
        random_t.sort()
        random_rf = bezier_ftov_parameters["min_vdee"]+np.random.random(bezier_ftov_parameters["nb_steps"])*(bezier_ftov_parameters["max_vdee"]-bezier_ftov_parameters["min_vdee"])

        nodes = np.vstack([random_t, random_rf])
        curve = bezier.Curve(nodes.T, degree=1)
        rf_setpoint=np.ones(len(self.fdee))*srseu_parameters["vdee_atten"]/srseu_parameters["voltage_factor"]

        t = np.linspace(0, 1,end_rf_index-start_rf_index)
        points = curve.evaluate_multi(t)
        rf_setpoint[start_rf_index:end_rf_index]=points[:,1]
        filtered_vdee=self.filter_vdee(rf_setpoint,srseu_parameters)
        t_sampling = np.linspace(0, 1, bezier_ftov_parameters["nb_steps"])
        nn_output=curve.evaluate_multi((t_sampling))
        return nn_output[:,1],np.vstack([self.time_vector,self.fdee,rf_setpoint,filtered_vdee]).T

    def extract_ftov(self,data,srseu_parameters):
        max_frequency_index=np.argmax(data[:,1])
        min_frequency_index=np.argmin(data[:,1])
        cropped_data=data[max_frequency_index:min_frequency_index,[1,3]]
        #Sort along the frequency axis
        sorted_data_index=np.argsort(cropped_data[:,0],axis=0)
        
        frequencies_digits=np.arange(3001)
        frequencies_mhz=(frequencies_digits+srseu_parameters["fdee_base_index"])/100
        voltages=np.interp(frequencies_mhz,cropped_data[sorted_data_index,0],cropped_data[sorted_data_index,1])*srseu_parameters["voltage_factor"]
        return np.vstack([frequencies_digits,voltages]).T.astype('int')
