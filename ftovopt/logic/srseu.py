import numpy as np
import requests
from requests.auth import HTTPBasicAuth
from io import StringIO
from requests_toolbelt.multipart.encoder import MultipartEncoder
import time
import re
class Srseu:

    def __init__(self,config):
        self.config=config["srseu"]
        self.auth=HTTPBasicAuth('iba', 'iba')
        self.ip=self.config["ip"]

    def loadFtoV(self):
        ftov_buffer=""
        url=f"http://{self.ip}/FtoV.csv"
        response = requests.get(url, stream = True,auth=self.auth)

        for chunk in response.iter_content(chunk_size=1024):
            ftov_buffer+=chunk.decode('utf-8')
        
        result=np.genfromtxt(StringIO(ftov_buffer),delimiter=';')
        return result

    def loadFtoG(self):
        ftog_buffer = ""
        url = f"http://{self.ip}/FtoG.csv"
        response = requests.get(url, stream=True, auth=self.auth)

        for chunk in response.iter_content(chunk_size=1024):
            ftog_buffer += chunk.decode('utf-8')

        result = np.genfromtxt(StringIO(ftog_buffer), delimiter=';')
        return result

    def sendFtoV(self,raw_ftov):
        ftov_buffer=StringIO()
        np.savetxt(ftov_buffer,raw_ftov,delimiter=';',fmt='%i')

        response=requests.get(f"http://{self.ip}/FtoV.cgi",auth=self.auth)

        mp_encoder = MultipartEncoder(
            fields={
                'File': ('FtoV.csv', ftov_buffer.getvalue(), 'text/csv'),
            }
        )

        response = requests.post(
            f'http://{self.ip}/ResFtoV.cgi',
            data=mp_encoder,
            headers={
                'Content-Type': mp_encoder.content_type, 
                "Upgrade-Insecure-Requests": "1",
                "DNT": "1",
                "Cache-Control":"max-age=0",
                "Referer": f"http://{self.ip}/FtoV.cgi"},
            auth=self.auth
        )

        while(True):
            response=requests.get(f"http://{self.ip}/updFtoV.cgi",auth=self.auth)
            if (response.content==b'<html><body style=body>Update finished</body></html>'):
                break
            time.sleep(0.2)

    def sendFtoG(self,raw_ftog):
        ftog_buffer=StringIO()
        np.savetxt(ftog_buffer,raw_ftog,delimiter=';',fmt='%i')

        response=requests.get(f"http://{self.ip}/FtoG.cgi",auth=self.auth)

        mp_encoder = MultipartEncoder(
            fields={
                'File': ('FtoG.csv', ftog_buffer.getvalue(), 'text/csv'),
            }
        )

        response = requests.post(
            f'http://{self.ip}/ResFtoG.cgi',
            data=mp_encoder,
            headers={
                'Content-Type': mp_encoder.content_type,
                "Upgrade-Insecure-Requests": "1",
                "DNT": "1",
                "Cache-Control":"max-age=0",
                "Referer": f"http://{self.ip}/FtoG.cgi"},
            auth=self.auth
        )

        while(True):
            response=requests.get(f"http://{self.ip}/updFtoG.cgi",auth=self.auth)
            if (response.content==b'<html><body style=body>Update finished</body></html>'):
                break

            time.sleep(0.2)

    def getAllParameters(self):
        result={}
        self.getParameters("filter.cgi",result)
        self.getParameters("pw_d.cgi",result)
        self.getParameters("param.cgi",result)

        return result

    def getParameters(self,page,parameters):
        response=requests.get(f"http://{self.ip}/{page}",auth=self.auth)

        these_regex='<input type=text name=(.+?) value="(.+?)" size=.+? maxlength=.+?>'
        #these_regex='<tr><td>(.+?)</td><td> : <input .+ value="(.+?)" .+?</tr>'
        pattern=re.compile(these_regex)
        x=re.findall(pattern,str(response.content))
        for i in x:
            parameters[i[0]]=int(i[1])


    def setVdeeAtt(self,amplitude):
        response=requests.get(f"http://{self.ip}/param.cgi?vdatt={amplitude}",auth=self.auth)
