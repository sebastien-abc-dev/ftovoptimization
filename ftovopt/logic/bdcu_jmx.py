import logging
import jpype
from jpype import java
from jpype import javax
import numpy as np
from jpype.types import *
import pandas as pd
import time

logger = logging.getLogger(__name__)

class BdcuJMX:

    def __init__(self, config):
        self.config = config["bdcu_jmx"]
        try:
            jpype.startJVM(jpype.getDefaultJVMPath(), "-ea")
        except:
            logger.error("Error with the local JVM")
    def connect(self):
        logger.info("Connection to JMX")
        try:
            self.jmx_url = javax.management.remote.JMXServiceURL(self.config["url"])
            self.jmx_connection = javax.management.remote.JMXConnectorFactory.connect(self.jmx_url)
            self.jmx_beam_connection = self.jmx_connection.getMBeanServerConnection()

            logger.info("Connected to JMX")

        except:
            logger.error("Error during connection to JMX")

    def close(self):
        self.jmx_connection.close()
        logger.info("Connexion to JMX closed")

    def build_default_map(self, nb_elements):
        result = pd.DataFrame()
        for i in range(len(self.config["channels"])):
            result[self.config["channels"][i]] = [self.config["default_values"][i]] * (nb_elements+1) + \
                                                 [self.config["default_stopping_elements_values"][i]] *\
                                                 (self.config["nb_stopping_elements"]+1)

        return result

    def convert_pandas_to_jmap(self,dataframe):
        python_map = {}
        for col in dataframe.columns:
            channel_values = JFloat[len(dataframe)]
            channel_values[:] = dataframe[col].values.tolist()
            python_map[col] = channel_values
        return java.util.HashMap(python_map)

    def send_burst(self, dataframe):
        bean_name = self.config["bean_name"]
        jmap = self.convert_pandas_to_jmap(dataframe)
        nb_elements = len(dataframe)-1-self.config["nb_stopping_elements"]
        jparameters = JObject[3]
        jparameters[:] = [JInt @ nb_elements, JInt @ self.config["nb_stopping_elements"], java.util.HashMap(jmap)]
        self.jmx_beam_connection.invoke(javax.management.ObjectName(bean_name), "sendBurstData",
                                        jparameters,["int", "int", "java.util.Map"])
        self.jmx_beam_connection.invoke(javax.management.ObjectName(bean_name), "scan", [], [])
        logs = self.jmx_beam_connection.invoke(javax.management.ObjectName(bean_name), "getLogs", [], [])

        logs_dict = dict(logs)
        logs_df = pd.DataFrame()
        for key in logs_dict:
            logs_df[f"{key}"] = np.array(logs_dict[key])

        return logs_df

    def send_cyclo_burst(self,setpoints):
        nb_pulses=setpoints.shape[0]
        map=self.build_default_map(nb_pulses)
        map.loc[0:nb_pulses-1,"SRSEU_VDEE"]=setpoints[:, 0]
        map.loc[0:nb_pulses-1,"SRSEU_IARC"]=setpoints[:, 1]
        logs=self.send_burst(map)
        result=logs.loc[2:2+nb_pulses-1,["IC_CYCLO_SMALL_GAP_MIN","IC_CYCLO_LARGE_GAP_MIN","DEGRADER_BS_MIN","SRSEU_FEEDBACK_VDEE_MIN","SRSEU_FEEDBACK_IARC_MIN"]].values
        result[:,0]=result[:,0]/self.config["ic_cyclo_gain"]/self.config["ic_cyclo_small_gap"]
        result[:,1]=result[:,1]/self.config["ic_cyclo_gain"]/self.config["ic_cyclo_large_gap"]

        return result