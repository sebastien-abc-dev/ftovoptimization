import logging
import socket
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

logger = logging.getLogger(__name__)


class BdcuBlak:
    def __init__(self,config):
        self.config = config["bdcu_blak"]
        self.sock = None

    def connect(self):
        logger.info("Connection to BLAK")
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            print(self.config["ip"])
            self.sock.connect((self.config["ip"], self.config["port"]))
            logger.info("Connected to BLAK")
        except socket.error:
            logger.error("Error")

    def close(self):
        self.send_command("stop; ")
        self.sock.close()

    def initialize(self):
        answer = self.send_command("initialize; ")
        logger.debug(answer)

    def bps_irradiation(self, input_map):
        com = f"bpsIrradiate; {list(input_map[:,0])}; {list(input_map[:,1])}"
        logger.debug(f"length of command: {len(com)}")
        raw_logs = self.send_command(com)
        lines_raw_logs = len(raw_logs.split("\n"))
        logger.debug(f"length of answer: {len(raw_logs)} | number of line {lines_raw_logs}")
        if not raw_logs.split("\n")[0].startswith("Failed;") or \
                not raw_logs.split("\n")[0].startswith("closing connection"):
            df = self.parse_bps_irradiation_logs(raw_logs)
            # suppose that the map always start with vdee setpoint > 0%
            ind = df[df["SRSEU_SETPOINT_FEEDBACK_VDEE"] > 0].index[0]
            nbpulse = len(input_map[:,0])
            logger.info(f"number of pulse expected: {nbpulse} -> number of pulse detected: {len(df)}")
            return np.array(df[["IC_CYCLO_SMALL_GAP","IC_CYCLO_LARGE_GAP","DEGRADER_BEAMSTOP"]])[ind:ind+nbpulse]
        else:
            logger.error(f"bps irradiation failed: {raw_logs}")
            return None

        # nb_ghost_beam_pulses=self.config["nb_ghost_beam_pulses"]
        # nb_cleanup_pulses=self.config["nb_cleanup_pulses"]
        # nb_repetition_in_map=self.config["nb_repetition_in_map"]
        # nb_accelerated_pulses=self.config["nb_accelerated_pulses"]
        #
        # return np.random.rand(nb_repetition_in_map*(nb_accelerated_pulses+nb_ghost_beam_pulses+nb_cleanup_pulses),3)

    def parse_bps_irradiation_logs(self, raw_logs):
        header_line = 0
        datas = []
        for r, raw in enumerate(raw_logs.split("\n")):
            if raw.startswith("#ELEMENT_ID"):
                header_line = r
                continue
            if header_line > 0 and raw != '':
                datas.append(raw.split(','))

        df = pd.DataFrame.from_records(datas, columns=raw_logs.split("\n")[header_line].lstrip('#').split(',')).astype(float)
        return df

    def send_command(self, command):
        to_send = f"{len(command)};{command}"
        while len(to_send) > 1000:
            paket = to_send[:1000]
            to_send = to_send[1000:]
            self.sock.sendall(paket.encode('ascii'))

        self.sock.sendall(to_send.encode('ascii'))

        buffer = self.sock.recv(1024).decode('ascii')
        expected_len_result = int(buffer.split(";")[0])
        logger.debug(f"expected length of result: {expected_len_result}")
        result = buffer[buffer.find(";") + 1:]
        while len(result) < expected_len_result:
            buffer = self.sock.recv(1024).decode('ascii')
            result += buffer

        logger.debug(f"length of result: {len(result)}")

        return result
