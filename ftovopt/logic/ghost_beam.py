import logging
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

logger = logging.getLogger(__name__)


class GhostBeam:

    def __init__(self,config):
        self.config=config["ghost_beam"]

    def build_setpoints(self):
        nb_ghost_beam_pulses=self.config["nb_ghost_beam_pulses"]
        nb_cleanup_pulses=self.config["nb_cleanup_pulses"]
        nb_repetition_in_map=self.config["nb_repetition_in_map"]
        nb_accelerated_pulses=self.config["nb_accelerated_pulses"]

        acceleration=np.zeros((nb_accelerated_pulses,2))
        acceleration[:,0]=self.config["rf_setpoint_acceleration"]
        acceleration[:,1]=self.config["iarc_setpoint_acceleration"]

        ghost_beam=np.zeros((nb_ghost_beam_pulses,2))
        ghost_beam[:,0]=self.config["rf_setpoint_ghost"]
        ghost_beam[:,1]=self.config["iarc_setpoint_ghost"]

        cleanup=np.zeros((nb_cleanup_pulses,2))
        cleanup[:,0]=self.config["rf_setpoint_cleanup"]
        cleanup[:,1]=self.config["iarc_setpoint_cleanup"]

        one_measure=np.vstack([acceleration,ghost_beam,cleanup])

        return np.tile(one_measure,(nb_repetition_in_map,1))


    def parse_ghost_beam_logs(self,logs):
        #Expected format:
        #output_logs[:,0]: ic_cyclo_small_gap - in protons
        #output_logs[:,1]: ic_cyclo_large_gap - in protons
        #output_logs[:,2]: beam_stop - in protons
        #output_logs[:,3]: vdee feedbacks at capture - in V
        #output_logs[:,4]: iarc feedbacks at capture - in V


        max_pulses=[]
        ghost_beams=[]

        nb_ghost_beam_pulses=self.config["nb_ghost_beam_pulses"]
        nb_cleanup_pulses=self.config["nb_cleanup_pulses"]
        nb_repetition_in_map=self.config["nb_repetition_in_map"]
        nb_accelerated_pulses=self.config["nb_accelerated_pulses"]

        for i in range(nb_repetition_in_map):
            frame_size=(nb_accelerated_pulses+nb_ghost_beam_pulses+nb_cleanup_pulses)
            max_pulses.append(logs[i*frame_size:i*frame_size+nb_accelerated_pulses,0])
            ghost_beams.append(logs[i*frame_size+nb_accelerated_pulses:i*frame_size+nb_accelerated_pulses+nb_ghost_beam_pulses,1])

        max_pulse_average=np.vstack(max_pulses).mean()
        ghost_beam=np.mean(np.vstack(ghost_beams),axis=0)

        x=np.arange(self.config["nb_ghost_beam_pulses"])*1E-3
        #ghost_beam=5E-12*np.exp(-x/300E-3)+np.random.randn(self.config["nb_ghost_beam_pulses"])*1E-12
        fit=np.zeros(self.config["nb_ghost_beam_pulses"])
        return (max_pulse_average,np.vstack([x,ghost_beam,fit]).T)


    def fit_objective(self,t,a,b):
        return a*np.exp(-t/b)

    def fit_function(self,ghost_beam):
        popt, _ = curve_fit(self.fit_objective, ghost_beam[:,0], ghost_beam[:,1],p0=[8E-15,10])
        ghost_beam[:,2]=self.fit_objective(ghost_beam[:,0],*popt)
        return popt