from pathlib import Path
import numpy as np
import bezier
import random

class FtoGGenerator:
    def __init__(self):
        pass

    def generate_flat_ftog(self,srseu_parameters,gain):
        frequencies=(np.arange(3001)+srseu_parameters["fdee_base_index"])/100.0
        gains=np.ones(3001)*gain
        return np.vstack([frequencies,gains]).T

    def generate_triangle_ftog(self,srseu_parameters,min_gain,max_gain):
        frequencies=(np.arange(3001)+srseu_parameters["fdee_base_index"])/100.0
        gains=np.linspace(min_gain,max_gain,num=3001)
        return np.vstack([frequencies,gains]).T

    def extract_ftog(self,data,srseu_parameters):

        frequencies_digits = np.arange(3001)
        gains_digits=data[:,1]/srseu_parameters["max_gain_for_ftog"]*32767.0
        return np.vstack([frequencies_digits, gains_digits]).T.astype('int')


    def computeFtoG(self,raw_ftog,srseu_parameters):
        frequencies=(np.arange(3001)+srseu_parameters["fdee_base_index"])/100.0
        gains=raw_ftog[:,1]/32767.0*srseu_parameters["max_gain_for_ftog"]
        return np.vstack([frequencies,gains]).T
